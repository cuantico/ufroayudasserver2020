package cl.ufro.dao;

import java.util.ArrayList;
import java.util.List;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class AyudantiaDao extends ObjetoDao {
	public Ayudantia find (Ayudantia registro){
		return (Ayudantia)super.find(registro);
	}
	public Ayudantia findById(Ayudantia ayudantia){
		List<ObjetoBd> lista = listAll();
		for(int i=0;i<lista.size();i++){
			Ayudantia aux = (Ayudantia)lista.get(i);
			if( aux.getId()==ayudantia.getId())
				return aux;	
		}
		return null;
	}
	public List<Ayudantia> listByMateria(Ayudantia ayudantia){
		List<ObjetoBd> lista = listAll();
		System.out.println("SIZE:"+lista.size());
		List<Ayudantia> nueva = new ArrayList<Ayudantia>();
		for(int i=0;i<lista.size();i++){
			Ayudantia aux = (Ayudantia)lista.get(i);
			System.out.println("Dao1::"+aux.getMateria()+"::"+ayudantia.getMateria());
			if( aux.getMateria().equals(ayudantia.getMateria())){
				nueva.add(aux);	
			System.out.println("Dao2::"+aux.getMateria());
			}
		}
		return nueva;
	}
	
	public Ayudantia findFirst(){
		List<ObjetoBd> lista = listAll();//los elementos de lista van a ser del tipo objetoBd 
		if(lista.size()>0){
			return (Ayudantia)(lista.get(0));//retorna el primero de la lista
		}else 		
		return null;
	}public Ayudantia findLast(){
		List<ObjetoBd> lista = listAll();//los elementos de lista van a ser del tipo objetoBd 
		if(lista.size()>0){
			return (Ayudantia)(lista.get(lista.size()-1));//return el ultimo elemento
		}else 		
		return null;
	}public Ayudantia agregar(Ayudantia ayudantia){
		Ayudantia ayudantia2= findLast();
		int id =1;
		if(ayudantia2!=null)
			id = ayudantia2.getId()+1;
		ayudantia.setId(id);
		add(ayudantia);
		return ayudantia;
	}
}
