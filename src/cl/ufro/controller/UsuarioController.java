package cl.ufro.controller;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.service.UsuarioService;

public class UsuarioController {
	private UsuarioService usuarioService = new UsuarioService();
	
	public Usuario identificar(Usuario usuario){
		return usuarioService.identificar(usuario);
	}
	public void actualizar(Usuario usuario){
		usuarioService.actualizar(usuario);
	}
	public Usuario findByCorreo(Usuario usuario){
		return usuarioService.findByCorreo(usuario);
	}
	public void guardar(Usuario usuario){
		usuarioService.guardar(usuario);
	}
	
}
