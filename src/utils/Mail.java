package utils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public abstract class Mail {
	
	public static boolean sendMail(String asunto, String mensaje, String[] destinos){
		return sendMail(asunto, mensaje, destinos, "");
	}

	public static boolean sendMail(String asunto, String mensaje, String[] destinos, String filename){
		try{
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.starttls.enable", "true");
			props.setProperty("mail.smtp.port", "587");
			props.setProperty("mail.smtp.user", ConfigMail.usuario);
			props.setProperty("mail.smtp.auth", "true");
			Session session = Session.getDefaultInstance(props, null);
			MimeMultipart multiParte = new MimeMultipart();
			//Se crea la lista de receptores
			Address [] receptores = new Address [destinos.length];
			for(int i=0;i<destinos.length;i++)
				receptores[i] = new InternetAddress (destinos[i]);
			MimeMessage message = new MimeMessage(session);
			// Se define el origen
			message.setFrom(new InternetAddress(ConfigMail.usuario));
			// Se agregan los destinatarios
			message.addRecipients(Message.RecipientType.BCC,receptores);
			// Se especifica el asunto
			message.setSubject(asunto);

			//Se agrega el contenido al email
			BodyPart contenido = new MimeBodyPart();
			contenido.setText(mensaje);
			multiParte.addBodyPart(contenido);

			//Se agrega el archivo adjunto al email si se especifica
			if(!(filename.equals(""))){
				BodyPart archivoAdj = new MimeBodyPart();
				DataSource source = new FileDataSource(filename);
				archivoAdj.setDataHandler(new DataHandler(source));
				archivoAdj.setFileName(filename);
				multiParte.addBodyPart(archivoAdj);
			}
			//Se agregan las "partes" del correo
			message.setContent(multiParte);

			//Se envía el correo
			Transport t = session.getTransport("smtp");
			t.connect(ConfigMail.usuario, ConfigMail.passwd);
			t.sendMessage(message, message.getAllRecipients());
			t.close();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}    
	}
}
