package cl.ufro.dao;


import java.util.List;

import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class UsuarioDao extends ObjetoDao{
	
	public Usuario find(Usuario registro){
		return (Usuario)super.find(registro);
	}

	public Usuario findByCorreo(Usuario usuario){
		List<ObjetoBd> lista = listAll();
		for(int i=0; i<lista.size();i++){
			Usuario aux = (Usuario)lista.get(i);
			if(aux.getCorreo().equals(usuario.getCorreo()))
				return aux;
		
		}
		return null;
		
		
		
	}
}
