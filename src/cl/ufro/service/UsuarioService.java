package cl.ufro.service;


import cl.ufro.bd.Usuario;
import cl.ufro.dao.UsuarioDao;

public class UsuarioService {
	private UsuarioDao usuarioDao = new UsuarioDao();
	
	/*Funcion identificar:
	 * Recibe un usuario por parametros,se llama al Dao,un usuarioDao es retornado para comparar
	 * si el usuario registrado coincide con el pin que se le ha asignado.
	 * retorna el usuario registrado.(nulo en caso de que no coincida el pin).
	 * 
	 * */
	public Usuario identificar(Usuario usuario){
		Usuario usuarioBD=(Usuario)usuarioDao.findByCorreo(usuario);          
		if(usuarioBD!=null && usuarioBD.getPin().equals(usuario.getPin()))
			return usuarioBD;
		return null;
		
	}
	
	/*Funcion findByCorreo:
	 * Busca en el archivo local por correo.
	 * 
	 * */
	public Usuario findByCorreo(Usuario usuario){
		return usuarioDao.findByCorreo(usuario);
		
	}
	/*Funcion actualizar:
	 * Accede al Dao para que se actualicen los campos.
	 * 
	 * */
	public void actualizar(Usuario usuario) {
		usuarioDao.update(usuario);         //la responsabilidad es del dao
	}
	/*Funcion guardar:
	 * Accede al Dao para guardar el usuario.
	 * 
	 * */
	public void guardar(Usuario usuario){
		usuarioDao.add(usuario);
	}
}