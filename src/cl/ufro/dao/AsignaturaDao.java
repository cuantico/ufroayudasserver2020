package cl.ufro.dao;

import java.util.ArrayList;
import java.util.List;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class AsignaturaDao extends ObjetoDao{
	
	
	public Asignatura find(Asignatura registro){
		return (Asignatura)super.find(registro);
	}
	
	
	public List<Asignatura> list(){ 
		List<ObjetoBd> lista = listAll();
		List<Asignatura> nueva = new ArrayList<Asignatura>();
		for(int i=0;i<lista.size();i++){
			Asignatura aux = (Asignatura)lista.get(i);
			nueva.add(aux);
		}
		return nueva;		
	}

}
