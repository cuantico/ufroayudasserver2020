package cl.ufro.dao;

import java.util.ArrayList;
import java.util.List;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class MuroDao extends ObjetoDao {
	public Muro find (Muro registro){
		return (Muro)super.find(registro);
	}
	public List<Muro> list(){ 
		List<ObjetoBd> lista = listAll();
		List<Muro> nuevo = new ArrayList<Muro>();
		for(int i=0;i<lista.size();i++){
			Muro aux = (Muro)lista.get(i);
			nuevo.add(aux);
		}
		return nuevo;		
	}

	public List<Muro> listByAyudantia(Ayudantia ayudantia){
		List<ObjetoBd> lista = listAll();
		List<Muro> nueva = new ArrayList<Muro>();
		for(int i=0;i<lista.size();i++){
			Muro aux = (Muro)lista.get(i);
			if( aux.getIdAyudantia()==ayudantia.getId())
				nueva.add(aux);	
		}
		return nueva;
	}/*
	public List<Muro> listByLastMsj(Ayudantia ayudantia){
		List<ObjetoBd> lista = listAll();
		List<Muro> nueva = new ArrayList<Muro>();
		for(int i=0;i<lista.size();i++){
			Muro aux = (Muro)lista.get(i);
			if( aux.getIdMsj()==ayudantia.getIdLastMsj())
				nueva.add(aux);	
		}
		return nueva;
	}*/
	public Muro findLast(Ayudantia ayudantia){
		List<Muro> lista = listByAyudantia(ayudantia);
		Muro nuevo = new Muro();
		nuevo.setIdMsj(-1);
		for(int i=0;i<lista.size();i++){
			Muro aux = (Muro)lista.get(i);
			if( aux.getIdMsj()>nuevo.getIdMsj())
				nuevo = aux;
		}
	return nuevo;	
}


public List<Muro> listByUser(Usuario user){
	List<ObjetoBd> lista = listAll();
	List<Muro> nueva = new ArrayList<Muro>();
	for(int i=0;i<lista.size();i++){
		Muro aux = (Muro)lista.get(i);
		if( aux.getCorreo()==user.getCorreo())
			nueva.add(aux);	
	}
	return nueva;
}

public Muro findLast(){
	List<ObjetoBd> lista = listAll();//los elementos de lista van a ser del tipo objetoBd 
	if(lista.size()>0){
		return (Muro)(lista.get(lista.size()-1));//retorna el primero de la lista
	}else 		
		return null;
}
public Muro agregar(Muro muro){
	Muro aux = findLast();
	int id =1;
	if(aux!=null)
		id = aux.getIdMsj()+1;
	muro.setIdMsj(id);
	add(muro);
	return muro;
}


}
