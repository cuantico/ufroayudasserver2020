package cl.ufro.service;

import java.util.Iterator;
import java.util.List;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;

import cl.ufro.dao.MuroDao;

public class MuroService {
	private MuroDao muroDao = new MuroDao();



	/*public void actualizar(Muro muro) {
		muroDao.update(muro);
	}
	public Muro identificar(Muro muro){
		Muro muroBD = muroDao.findById(muro); //BUSCAMOS POR ID
		if(muroBD!=null && muroBD.getIdAyudantia()==muro.getIdAyudantia())
			return muroBD;
		return null;		
	}
	public Muro findById(Muro muro){
		return muroDao.findById(muro);
	}
	 */
	public Muro guardar(Muro muro){
		return muroDao.agregar(muro);
	}
	
	public  List<Muro> listar(Ayudantia ayudantia){
		return muroDao.listByAyudantia(ayudantia);
	}
	public  List<Muro> listar(Usuario user){
		return muroDao.listByUser(user);
	}
	public Muro findLast(Ayudantia ayudantia){
		return muroDao.findLast(ayudantia);
	}
	}


/*
	public Notificacion notificacion(Ayudantia ayudantia){
		Notificacion n = new Notificacion();
		List<Muro> lista = muroDao.listByAyudantia(ayudantia);
		for(int i=0;i<lista.size();i++){
			Muro aux = (Muro)lista.get(i);
			if(aux.getIdMsj()>ayudantia.getIdLastMsj()){
				n.setR(1);
				return n;
			}
		}
		n.setR(0);
		return n;
	}*/


