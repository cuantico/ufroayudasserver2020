package cl.ufro.service;
import java.util.List;

import cl.ufro.bd.Asignatura;
import cl.ufro.dao.AsignaturaDao;

public class AsignaturaService {

	private AsignaturaDao asignaturaDao = new AsignaturaDao();

	public List<Asignatura> listar(){
		return asignaturaDao.list();
	}
	public void guardar (Asignatura asignatura){
		asignaturaDao.add(asignatura);
	}
}

